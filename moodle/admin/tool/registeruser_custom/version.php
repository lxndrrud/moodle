<?php
/**
 * Plugin version info.
 *
 * @package    tool_registeruser_custom
 * @copyright  2021 lxndrrud
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2021112000;            // The current plugin version (Date: YYYYMMDDXX).
$plugin->requires  = 2021051100;            // Requires this Moodle version.
$plugin->component = 'tool_registeruser_custom';   // Full name of the plugin (used for diagnostics).